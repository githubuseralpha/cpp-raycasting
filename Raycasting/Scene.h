#pragma once
#include "Color.h"
#include "Geometry.h"
#include <vector>
#include "SDL.h"


class SceneElement {
public:
	SceneElement(Face t_face, Color t_color) : m_face(t_face), m_color(t_color) {};
	const Face& getFace() const { return m_face; };
	const Color& getColor() const { return m_color; };

private:
	Face m_face;
	Color m_color;
};

class Scene {
public:
	Scene(float t_x, float t_y, float t_z);
	void addFace(Vector3 t_position, Vector3 t_edge1, Vector3 t_edge2, Color color);
	std::vector<SceneElement>& getObjects();
	const Vector3& getLightSource() const { return m_lightSource; };
private:
	std::vector<SceneElement> m_objects;
	Vector3 m_lightSource;
};

class Screen {
public:
	virtual void display() = 0;
	virtual void colorPixel(int t_x, int t_y, Color t_color) = 0;
	virtual int getWidth() = 0;
	virtual int getHeigt() = 0;
};

class ConsoleScreen : public Screen {
private:
	int m_width, m_height;
	Color ***m_screen_surface;
public:
	ConsoleScreen(unsigned int t_width, unsigned int t_height);
	~ConsoleScreen();
	void display();
	void colorPixel(int t_x, int t_y, Color t_color);
	int getWidth() { return m_width; }
	int getHeigt() { return m_height; }
};

class SDLScreen : public Screen {
private:
	int m_width, m_height;
	int *m_screen_surface;
	SDL_Window* m_window;
	SDL_Renderer* m_renderer;

	int RMASK = 0xff000000;
	int GMASK = 0x00ff0000;
	int BMASK = 0x0000ff00;
public:
	SDLScreen(unsigned int t_width, unsigned int t_height);
	~SDLScreen();
	void display();
	void colorPixel(int t_x, int t_y, Color t_color);
	int getWidth() { return m_width; }
	int getHeigt() { return m_height; }
};

class Camera {
public:
	Camera(Scene* t_scene, Screen* t_screen,
		float m_real_width, float m_real_height, float m_focal);
	void generateView();
	void rotate(const float yaw, const float pitch, const float roll);
private:
	const float RAY_RANGE = 1000;
	Scene* m_scene;
	Vector3 m_position;
	Rotation m_rotation;
	Screen *m_screen;
	float m_realWidth, m_realHeight, m_focal;
};


