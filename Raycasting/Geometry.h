#pragma once
#include <iostream>
#include <array>

class Vector3 {
public:
	Vector3(float t_x, float t_y, float t_z) : m_x(t_x), m_y(t_y), m_z(t_z) {};
	Vector3() : m_x(0), m_y(0), m_z(0)  {};
	~Vector3() {};
	float getLength() const;
	Vector3 operator+(const Vector3& t_other) const;
	Vector3 operator-(const Vector3& t_other) const;
	Vector3 operator*(float t_scalar);
	Vector3 scale(const float t_scalar) const;
	Vector3 rotateX(const float angle) const;
	Vector3 rotateY(const float angle) const;
	Vector3 rotateZ(const float angle) const;
	Vector3 rotate(const float angleX, const float angleY, const float angleZ) const;

	float dotProduct(const Vector3& t_other) const;
	Vector3 crossProduct(const Vector3& t_other) const;
	Vector3 getUnitVector() const;
	void norm() { m_x = m_x / getLength(); m_y = m_y / getLength(); m_z = m_z / getLength(); };

	friend std::ostream& operator<<(std::ostream& t_stream, const Vector3& t_vec);
	float m_x, m_y, m_z;
};

class Face {
public:
	Face(const Vector3& t_vertice, const Vector3& t_edge1, const Vector3& t_edge2);
	~Face() {};
	const Vector3& getVertice() const { return m_vertice; };
	const std::array<Vector3, 3>& getVertices() const { return { m_vertice, m_vertice + m_edge1, m_vertice + m_edge2 }; };

	const Vector3& getNormal() const { return m_normal;  };
private:
	Vector3 m_vertice, m_edge1, m_edge2, m_normal;
};

class MyRay {
public:
	MyRay(const Vector3& t_position, const Vector3& t_direction, float length);
	MyRay(const Vector3& t_position, float t_dirX, float t_dirY, float t_dirZ, float length);
	bool computeIntersection(const Face& t_face, Vector3& out_hitPoint, Vector3& out_hitNormal) const;
	const Vector3& getPosition() const { return m_position; };
	const Vector3& getDirection() const { return m_direction; };
private:
	Vector3 m_position, m_direction;
	float m_length;
};

struct Rotation {
	float yaw, pitch, roll;
};

float signedVolume(Vector3 t_a, Vector3 t_b, Vector3 t_c, Vector3 t_d);
float signedVolume(Vector3 t_a, Vector3 t_n, Vector3 t_d);

Vector3 operator* (const Vector3& y, float x);
