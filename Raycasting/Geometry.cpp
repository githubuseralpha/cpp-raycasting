#include <math.h>
#include <ostream>
#include <array>
#include "Geometry.h"


float Vector3::getLength() const {
	return sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
}

Vector3 Vector3::operator+(const Vector3& t_other) const {
	return Vector3(m_x + t_other.m_x, m_y + t_other.m_y, m_z + t_other.m_z);
}

Vector3 Vector3::operator-(const Vector3& t_other) const {
	return Vector3(m_x - t_other.m_x, m_y - t_other.m_y, m_z - t_other.m_z);
}

Vector3 Vector3::scale(const float t_scalar) const {
	return Vector3(m_x * t_scalar, m_y * t_scalar, m_z * t_scalar);
}

Vector3 Vector3::operator*(float t_scalar) {
	return this->scale(t_scalar);
}

Vector3 operator*(const Vector3& y, float x) {
	return y.scale(x);
}

float Vector3::dotProduct(const Vector3& t_other) const {
	return m_x * t_other.m_x + m_y * t_other.m_y + m_z * t_other.m_z;
}

Vector3 Vector3::crossProduct(const Vector3& t_other) const {
	return Vector3(
		m_y * t_other.m_z - m_z * t_other.m_y,
		m_z * t_other.m_x - m_x * t_other.m_z,
		m_x * t_other.m_y - m_y * t_other.m_x
	);
}

Vector3 Vector3::rotate(const float angleX, const float angleY, const float angleZ) const {
	Vector3 rotVec(m_x, m_y, m_z);
	return rotVec.rotateX(angleX).rotateY(angleY).rotateZ(angleZ);
}


Vector3 Vector3::getUnitVector() const {
	return (*this) * (1.0 / getLength());
}

Vector3 Vector3::rotateX(const float angle) const {
	return Vector3(
		m_x, 
		m_y * cosf(angle) - m_z * sinf(angle),
		m_y * sinf(angle) + m_z * cosf(angle)
	);
}
Vector3 Vector3::rotateY(const float angle) const {
	return Vector3(
		m_x * cosf(angle) + m_z * sinf(angle),
		m_y,
		-m_x * sinf(angle) + m_z * cosf(angle)
	);
}
Vector3 Vector3::rotateZ(const float angle) const {
	return Vector3(
		m_x * cosf(angle) - m_y * sinf(angle),
		m_x * sinf(angle) + m_y * cosf(angle),
		m_z
	);
}

std::ostream& operator<<(std::ostream& t_stream, const Vector3& t_vec) {
	t_stream << "Vec(" << t_vec.m_x << ", " << t_vec.m_y << ", " << t_vec.m_z << ")";
	return t_stream;
};

Face::Face(const Vector3& t_vertice, const Vector3& t_edge1, const Vector3& t_edge2) :
	m_vertice(t_vertice), m_edge1(t_edge1), m_edge2(t_edge2), 
	m_normal(t_edge2.crossProduct(t_edge1).getUnitVector()) {};

MyRay::MyRay(const Vector3& t_position, const Vector3& t_direction, float length) :
	m_position(t_position), m_direction(t_direction), m_length(length) {};

MyRay::MyRay(const Vector3& t_position, float t_dirX, float t_dirY, float t_dirZ, float length) :
	m_position(t_position), m_direction(t_dirX, t_dirY, t_dirZ), m_length(length) {};

bool MyRay::computeIntersection(const Face& t_face, Vector3& out_hitPoint, Vector3& out_hitNormal) const {
	if (m_direction.dotProduct(t_face.getNormal()) > 0)
		return false;
	auto vertices = t_face.getVertices();
	Vector3 q1 = m_position;
	Vector3 q2 = m_position + m_direction.scale(m_length);
	Vector3 p1 = vertices[0];
	Vector3 p2 = vertices[1];
	Vector3 p3 = vertices[2];
	//std::cout << t_face.getNormal() << std::endl;

	if (signedVolume(q1, p1, p2, p3) * signedVolume(q2, p1, p2, p3) > 0)
		return false;
	float s1 = signedVolume(q1, q2, p1, p2);
	float s2 = signedVolume(q1, q2, p2, p3);
	float s3 = signedVolume(q1, q2, p3, p1);
	if (!((s1 >= 0 && s2 >= 0 && s3 >= 0) || (s1 < 0 && s2 < 0 && s3 < 0)))
		return false;

	float a = ((t_face.getVertice() - m_position).dotProduct(t_face.getNormal())) / (m_direction.dotProduct(t_face.getNormal()));
	out_hitPoint = q1 + m_direction * a;
	out_hitNormal = t_face.getNormal();
	return true;
}
/*
bool Ray::computeIntersection(const Face& t_face) const {
	if (m_direction.dotProduct(t_face.getNormal()) > 0)
		return false;
	auto vertices = t_face.getVertices();
	Vector3 q1 = m_position;
	Vector3 q2 = m_position + m_direction.scale(m_length);
	Vector3 p1 = vertices[0];
	Vector3 p2 = vertices[1];
	Vector3 p3 = vertices[2];
	if (signedVolume(q1, p1, p2, p3) * signedVolume(q2, p1, p2, p3) > 0)
		return false;
	float s1 = signedVolume(q1, q2, p1, p2);
	float s2 = signedVolume(q1, q2, p2, p3);
	float s3 = signedVolume(q1, q2, p3, p1);
	if (!((s1 >= 0 && s2 >= 0 && s3 >= 0) || (s1 < 0 && s2 < 0 && s3 < 0)))
		return false;
	return true;
}
*/

float signedVolume(Vector3 t_a, Vector3 t_b, Vector3 t_c, Vector3 t_d) {
	return ((t_b - t_a).crossProduct(t_c - t_a)).dotProduct(t_d - t_a) / 6;
}

float signedVolume(Vector3 t_a, Vector3 t_n, Vector3 t_d) {
	return t_n.dotProduct(t_d - t_a) / 6;
}
