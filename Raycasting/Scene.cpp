#include "Color.h"
#include "Scene.h"
#include <iostream>
#include <math.h>
#include <limits>

ConsoleScreen::ConsoleScreen(unsigned int t_width, unsigned int t_height) :
	m_width(t_width), m_height(t_height) {
	m_screen_surface = new Color**;
	*m_screen_surface = new Color*[t_height];
	for (int i = 0; i < t_height; i++) {
		(*m_screen_surface)[i] = new Color[t_width];
		for (int j = 0; j < t_width; j++) {
			(*m_screen_surface)[i][j] = Color{ 0, 0, 0 };
		}
	}
}

ConsoleScreen::~ConsoleScreen() {
	for (int i = 0; i < m_height; i++) {
		delete[] (*m_screen_surface)[i];
	}
	delete[] *m_screen_surface;
	delete m_screen_surface;
}

void ConsoleScreen::colorPixel(int t_x, int t_y, Color t_color) {
	(*m_screen_surface)[t_y][t_x] = t_color;
}

void ConsoleScreen::display() {
	std::cout << "P3\n" << m_width << ' ' << m_height << "\n255\n";
	for (int i = 0; i < m_height; i++) {
		for (int j = 0; j < m_width; j++) {
			int r = (*m_screen_surface)[i][j].r;
			int g = (*m_screen_surface)[i][j].g;
			int b = (*m_screen_surface)[i][j].b;
			std::cout << r << ' ' << g << ' ' << b << '\n';
		}
	}
}

Scene::Scene(float t_x, float t_y, float t_z):
	m_objects(std::vector<SceneElement>()), m_lightSource(t_x, t_y, t_z) {};

void Scene::addFace(Vector3 t_position, Vector3 t_edge1, Vector3 t_edge2, Color color) {
	Face face(t_position, t_edge1, t_edge2);
	SceneElement sceneElement(face, color);
	m_objects.push_back(sceneElement);
}

std::vector<SceneElement>& Scene::getObjects() {
	return m_objects;
}

Camera::Camera(Scene* t_scene, Screen* t_screen,
	float t_realWidth, float t_realHeight, float t_focal) :
	m_scene(t_scene), m_position(0, 0, 0), m_rotation(Rotation {0, 0, 0 }),
	m_screen(t_screen), m_realWidth(t_realWidth),
	m_realHeight(t_realHeight), m_focal(t_focal) {};

void Camera::rotate(const float yaw, const float pitch, const float roll) {
	m_rotation.yaw += yaw;
	m_rotation.roll += roll;
	m_rotation.pitch += pitch;
}


void Camera::generateView() {
	int pixelHeight = m_screen->getHeigt();
	int pixelWidth = m_screen->getWidth();
	float stepX = m_realWidth / pixelWidth;
	float stepY = m_realHeight / pixelHeight;

	for (int i = 0; i < pixelHeight; i++) {
		for (int j = 0; j < pixelWidth; j++) {
			float currX = -m_realWidth/2 + j * stepX;
			float currY = -m_realHeight/2 + i * stepY;
			Vector3 dir(currX, currY, m_focal);
			dir = dir.rotate(m_rotation.pitch, m_rotation.yaw, m_rotation.roll);
			dir.norm();
			MyRay currentRay(m_position, dir, RAY_RANGE);
			//if(currentRay.getDirection().m_y > 0)
				//std::cout << currentRay.getDirection() << std::endl;
			Vector3 hitPoint;
			Vector3 hitNormal;
			SceneElement* hitFace = nullptr;
			float minDistance = std::numeric_limits<float>::max();
			for (SceneElement& face : m_scene->getObjects()) {
				if (currentRay.computeIntersection(face.getFace(), hitPoint, hitNormal)) {
					float distance = (hitPoint - currentRay.getPosition()).getLength();
					if (distance < minDistance) {
						distance = minDistance;
						hitFace = &face;
					}
				}
			}
			bool isShadow = false;
			Vector3 v1, v2;
			if (hitFace) {
				Vector3 dir = (m_scene->getLightSource() - hitPoint);
				dir.norm();
				MyRay shadowRay(hitPoint, dir, RAY_RANGE);
				//std::cout << shadowRay.getDirection() << std::endl;
				//std::cout << shadowRay.getPosition() << std::endl;
				if (dir.dotProduct(hitNormal) < 0)
					isShadow = true;
				else {
					for (SceneElement& face : m_scene->getObjects()) {
						if (&face == hitFace)
							continue;
						if (currentRay.computeIntersection(face.getFace(), v1, v2)) {
							//std::cout << v1 << std::endl;
							isShadow = true;
							break;
						}
					}
				}
			}
			if (hitFace && !isShadow) {
				m_screen->colorPixel(j, i, hitFace->getColor());
			}
			else if (hitFace && isShadow) {
				m_screen->colorPixel(j, i, Color{ 0, 0, 0 });
			}
			else
				m_screen->colorPixel(j, i, Color{ 0, 125, 220 });
		}
	}
}


SDLScreen::SDLScreen(unsigned int t_width, unsigned int t_height) :
	m_width(t_width), m_height(t_height), m_screen_surface(new int[t_height * t_width]) {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		printf("error initializing SDL: %s\n", SDL_GetError());
	}
	m_window = SDL_CreateWindow("Example", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, t_width, t_height, SDL_WINDOW_SHOWN);
	m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(m_renderer, 255, 255, 255, 255);

	for (int i = 0; i < t_height * t_width; i++) {
		m_screen_surface[i] = 0;
	}

}

SDLScreen::~SDLScreen(){
	SDL_DestroyRenderer(m_renderer);
	SDL_DestroyWindow(m_window);
	
	delete[] m_screen_surface;
	std::cout << "destroyed";
}

void SDLScreen::display() {
	SDL_Texture* texture;
	SDL_Surface* surface = SDL_CreateRGBSurfaceFrom(m_screen_surface, m_width, m_height, 32, 4*m_height, RMASK, GMASK, BMASK, 0);
	texture = SDL_CreateTextureFromSurface(m_renderer, surface);
	SDL_FreeSurface(surface);
	SDL_SetRenderDrawColor(m_renderer, 255, 255, 255, 255);
	SDL_RenderClear(m_renderer);
	SDL_RenderCopyEx(m_renderer, texture, NULL, NULL, 0, NULL, SDL_FLIP_NONE);
	SDL_RenderPresent(m_renderer);
	SDL_DestroyTexture(texture);
}

void SDLScreen::colorPixel(int t_x, int t_y, Color t_color) {
	int color = 0x00000000;
	color += ((int)t_color.r) << 24;
	color += ((int)t_color.g) << 16;
	color += ((int)t_color.b) << 8;

	m_screen_surface[t_y * m_width + t_x] = color;
}
