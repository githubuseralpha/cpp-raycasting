﻿#include <iostream>
#include "Geometry.h"
#include "Scene.h"
#include "SDL.h"

int main(int args, char* argv[])
{
	{
		
		SDLScreen screen(500, 500);
		Scene scene(0, 0, 0);
		Camera camera(&scene, &screen, 1, 1, 0.7);
		scene.addFace(Vector3(0, 0, 10), Vector3(10, 0, 0), Vector3(0, 10, 0), { 250, 0, 0 });
		for (int i = 0; i < 10; i++) {
			camera.rotate(0, -0.1, 0);
			camera.generateView();
			screen.display();
			//SDL_Delay(1000);
		}
	}

	return 0;
}

